from flask import Blueprint, render_template, render_template_string, redirect, url_for
from flask import current_app as app
from flask_plugins import emit_event, connect_event
from app import AppPlugin
from flask_babel import lazy_gettext as _l
import os 
 
__plugin__ = "About"
__version__ = "1.0.0"


bp = Blueprint("about", __name__, template_folder="templates")

##-- ---[ About ]------------------------
def file_content(filepath):
  try:
    with open(filepath, 'r') as file:
      data = file.read()
  except IOError:
    return None

  return data
  
@bp.route("/")
def index():
  ## Plugins can add custom action to index route
  emit_event("about_route_on_start")
  
  basedir = app.config.get("BASEDIR")
  content = {
    'readme' : file_content(os.path.join(basedir, 'README.txt')),
    'todo' : file_content(os.path.join(basedir, 'TODO.txt')),
    'license' : file_content(os.path.join(basedir, 'LICENSE.txt')),
    'credits' : file_content(os.path.join(basedir, 'CREDITS.txt'))
  }
    
  return render_template("public/about.html", title=_l("About"), description=app.config.get("APP_DESC"), content=content )


@bp.route('/readme')
def readme():
    return redirect(url_for('.index',_anchor='readme'))

@bp.route('/license')
def license():
    return redirect(url_for('.index',_anchor='license'))

@bp.route('/credits')
def credits():
    return redirect(url_for('.index',_anchor='credits'))

@bp.route('/contact')
def contact():
    return redirect(url_for('.index',_anchor='contact'))

@bp.route('/bug')
def bug():
    return redirect(url_for('.index',_anchor='bug'))

@bp.route('/git')
def git():
    return redirect(url_for('.index',_anchor='git'))

##-- ---[ Register Plugin ]------------------------

def inject_footer_link_col_1():
    return render_template_string("""
    {% import "public/macros/navigation_macros.htm" as macro %}
    {{ macro.footer_link(_('Credits'),href=url_for('about.credits'), icon_class="glyphicon glyphicon-certificate") }} 
    {{ macro.footer_link(_('License'),href=url_for('about.license'), icon_class="glyphicon glyphicon-copyright-mark") }}
    """ )
def inject_footer_link_col_2():
    return render_template_string("""
    {% import "public/macros/navigation_macros.htm" as macro %}
    {{ macro.footer_link(_('Source Code'),href=url_for('about.git'), icon_class="fa fa-code", title=_("View source code on Gitlab")) }} 
    {{ macro.footer_link(_('Report a bug'),href=url_for('about.bug'), icon_class="fa fa-bug", title=_("Open an issue in the code source bug tracker")) }}
    """ )
def inject_footer_link_col_4():
    return render_template_string("""
    {% import "public/macros/navigation_macros.htm" as macro %}
    {{ macro.footer_link(_('Contact'),href=url_for('about.contact'), icon_class="glyphicon glyphicon-envelope", title=_("Send an e-mail")) }} 
    """ )




class About(AppPlugin):

    def setup(self):
        self.register_blueprint(bp, url_prefix="/about/")

        connect_event("footer_col_1", inject_footer_link_col_1)
        connect_event("footer_col_2", inject_footer_link_col_2)
        connect_event("footer_col_4", inject_footer_link_col_4)
