from flask import Blueprint, render_template
from flask import current_app as app
from flask_plugins import emit_event
import os 
bp = Blueprint("about", __name__)

##-- ---[ About ]------------------------
def file_content(filepath):
  try:
    with open(filepath, 'r') as file:
      data = file.read()
  except IOError:
    return None

  return data
  
@bp.route("/")
def index():
  ## Plugins can add custom action to index route
  emit_event("about_route_on_start")
  
  basedir = app.config.get("BASEDIR")
  content = {
    'readme' : file_content(os.path.join(basedir, 'README.txt')),
    'todo' : file_content(os.path.join(basedir, 'TODO.txt')),
    'license' : file_content(os.path.join(basedir, 'LICENSE.txt')),
    'credits' : file_content(os.path.join(basedir, 'CREDITS.txt'))
  }
    
  return render_template("public/about.html", title="About This App", description=app.config.get("APP_DESC"), content=content )


@bp.route('/readme')
def readme():
    return redirect(url_for('.index',_anchor='readme'))

@bp.route('/license')
def license():
    return redirect(url_for('.index',_anchor='license'))

@bp.route('/credits')
def credits():
    return redirect(url_for('.index',_anchor='credits'))

@bp.route('/contact')
def contact():
    return redirect(url_for('.index',_anchor='contact'))

@bp.route('/bug')
def bug():
    return redirect(url_for('.index',_anchor='bug'))

@bp.route('/git')
def git():
    return redirect(url_for('.index',_anchor='git'))

